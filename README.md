SC Erlang Challenge
=========================

Application implement a minimal Chrome DevTools Procotol client so you can listen for all outgoing connections happening on Coinbase inside Chrome using --remote-debugging-port.

Launching
---------

* Ensure that Erlang/OTP 21 or greater is installed and on PATH
* Ensure that Rebar 3 is on PATH
* Edit `env/default.config` and change `host` and `port` according to your environment
* Launch Rebar shell:
```
$ rebar3 shell
```
Video
---------
Also you can watch one minute video with all process
https://drive.google.com/file/d/1KwhzKyi3VXntQNziji6OxYxPhzEYeU1o/view?usp=sharing
