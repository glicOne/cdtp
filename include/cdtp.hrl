-define(COINBASE_URL, <<"https://pro.coinbase.com/trade/">>).

-record(socket_controller, {
			    page_id,
			    socket_connection,
			    timer
			   }).

-record(cdtp_db, {
		  table
		 }).
-define(ETS_CDTP, ets_cdtp).
