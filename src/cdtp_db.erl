-module(cdtp_db).
-author("glicOne").
-include_lib("kernel/include/logger.hrl").
-include("cdtp.hrl").
-behaviour(gen_server).

-export([start_link/0, update_price/2]).

-export([init/1,
	 handle_call/3,
	 handle_cast/2,
	 handle_info/2,
	 terminate/2,
	 code_change/3]).

-define(SERVER, ?MODULE).

%%%%%%%%%%%%%%%%%%%%%%%
%%     _   ___ ___   %%
%%    /_\ | _ \_ _|  %%
%%   / _ \|  _/| |   %%
%%  /_/ \_\_| |___|  %%
%%                   %%
%%%%%%%%%%%%%%%%%%%%%%%

update_price(ProductId, Price) ->
    gen_server:call(?SERVER, {update_price, ProductId, Price}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  ___ _            _        _   _                                    %%
%% / __| |_ __ _ _ _| |_ ___ | |_| |_  ___   ___ ___ _ ___ _____ _ _   %%
%% \__ \  _/ _` | '_|  _(_-< |  _| ' \/ -_) (_-</ -_) '_\ V / -_) '_|  %%
%% |___/\__\__,_|_|  \__/__/  \__|_||_\___| /__/\___|_|  \_/\___|_|    %%
%%                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(start_link() ->
	     {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   ___      _ _   _      _ _          %%
%%  |_ _|_ _ (_) |_(_)__ _| (_)______   %%
%%   | || ' \| |  _| / _` | | |_ / -_)  %%
%%  |___|_||_|_|\__|_\__,_|_|_/__\___|  %%
%%                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(init(Args :: term()) ->
	     {ok, State :: #cdtp_db{}} | {ok, State :: #cdtp_db{}, timeout() | hibernate} |
	     {stop, Reason :: term()} | ignore).
init([]) ->
    Table = ets:new(?ETS_CDTP, [set, named_table]),
    {ok, #cdtp_db{table = Table}}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   _  _              _ _ _              ___      _ _   __  __                               %%
%%  | || |__ _ _ _  __| | (_)_ _  __ _   / __|__ _| | | |  \/  |___ ______ __ _ __ _ ___ ___  %%
%%  | __ / _` | ' \/ _` | | | ' \/ _` | | (__/ _` | | | | |\/| / -_|_-<_-</ _` / _` / -_|_-<  %%
%%  |_||_\__,_|_||_\__,_|_|_|_||_\__, |  \___\__,_|_|_| |_|  |_\___/__/__/\__,_\__, \___/__/  %%
%%				  |___/                                         |___/         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
		  State :: #cdtp_db{}) ->
	     {reply, Reply :: term(), NewState :: #cdtp_db{}} |
	     {reply, Reply :: term(), NewState :: #cdtp_db{}, timeout() | hibernate} |
	     {noreply, NewState :: #cdtp_db{}} |
	     {noreply, NewState :: #cdtp_db{}, timeout() | hibernate} |
	     {stop, Reason :: term(), Reply :: term(), NewState :: #cdtp_db{}} |
	     {stop, Reason :: term(), NewState :: #cdtp_db{}}).
handle_call({update_price, ProductId, Price}, _, #cdtp_db{table = Table} = State) ->
    true = ets:insert(Table, {ProductId, Price}),
    ?LOG_INFO("~p: ~p~n", [ProductId, Price]),
    {reply, ok, State};
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   _  _              _ _ _              ___         _     __  __                               %%
%%  | || |__ _ _ _  __| | (_)_ _  __ _   / __|__ _ __| |_  |  \/  |___ ______ __ _ __ _ ___ ___  %%
%%  | __ / _` | ' \/ _` | | | ' \/ _` | | (__/ _` (_-<  _| | |\/| / -_|_-<_-</ _` / _` / -_|_-<  %%
%%  |_||_\__,_|_||_\__,_|_|_|_||_\__, |  \___\__,_/__/\__| |_|  |_\___/__/__/\__,_\__, \___/__/  %%
%%                                |___/                                            |___/         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(handle_cast(Request :: term(), State :: #cdtp_db{}) ->
	     {noreply, NewState :: #cdtp_db{}} |
	     {noreply, NewState :: #cdtp_db{}, timeout() | hibernate} |
	     {stop, Reason :: term(), NewState :: #cdtp_db{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   _  _              _ _ _              ___  _   _              __  __                               %%
%%  | || |__ _ _ _  __| | (_)_ _  __ _   / _ \| |_| |_  ___ _ _  |  \/  |___ ______ __ _ __ _ ___ ___  %%
%%  | __ / _` | ' \/ _` | | | ' \/ _` | | (_) |  _| ' \/ -_) '_| | |\/| / -_|_-<_-</ _` / _` / -_|_-<  %%
%%  |_||_\__,_|_||_\__,_|_|_|_||_\__, |  \___/ \__|_||_\___|_|   |_|  |_\___/__/__/\__,_\__, \___/__/  %%
%%                                |___/                                                  |___/         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(handle_info(Info :: timeout() | term(), State :: #cdtp_db{}) ->
	     {noreply, NewState :: #cdtp_db{}} |
	     {noreply, NewState :: #cdtp_db{}, timeout() | hibernate} |
	     {stop, Reason :: term(), NewState :: #cdtp_db{}}).
handle_info(_Info, State) ->
    {noreply, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This function is called by a gen_server when it is about to         %%
%% terminate. It should be the opposite of Module:init/1 and do any    %%
%% necessary cleaning up. When it returns, the gen_server terminates   %%
%% with Reason. The return value is ignored.                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
		State :: #cdtp_db{}) -> term()).
terminate(_Reason, _State) ->
    ok.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    ___         _        ___ _                         %%
%%   / __|___  __| |___   / __| |_  __ _ _ _  __ _ ___   %%
%%  | (__/ _ \/ _` / -_) | (__| ' \/ _` | ' \/ _` / -_)  %%
%%   \___\___/\__,_\___|  \___|_||_\__,_|_||_\__, \___|  %%
%%                                          |___/        %%
%% Convert process state when code is changed            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(code_change(OldVsn :: term() | {down, term()}, State :: #cdtp_db{},
		  Extra :: term()) ->
	     {ok, NewState :: #cdtp_db{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
