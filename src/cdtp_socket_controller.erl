-module(cdtp_socket_controller).
-author("glicOne").
-include_lib("kernel/include/logger.hrl").
-include("cdtp.hrl").
-behaviour(gen_server).

-export([start_link/2]).

-export([init/1,
	 handle_call/3,
	 handle_cast/2,
	 handle_info/2,
	 terminate/2,
	 code_change/3]).

-define(SERVER, ?MODULE).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  ___ _            _        _   _                                    %%
%% / __| |_ __ _ _ _| |_ ___ | |_| |_  ___   ___ ___ _ ___ _____ _ _   %%
%% \__ \  _/ _` | '_|  _(_-< |  _| ' \/ -_) (_-</ -_) '_\ V / -_) '_|  %%
%% |___/\__\__,_|_|  \__/__/  \__|_||_\___| /__/\___|_|  \_/\___|_|    %%
%%                                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                  

-spec(start_link(binary(), non_neg_integer()) -> {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link(Host, Port) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, {Host, Port}, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    ___         _        ___ _                         %%
%%   / __|___  __| |___   / __| |_  __ _ _ _  __ _ ___   %%
%%  | (__/ _ \/ _` / -_) | (__| ' \/ _` | ' \/ _` / -_)  %%
%%   \___\___/\__,_\___|  \___|_||_\__,_|_||_\__, \___|  %%
%%                                          |___/        %%
%% Convert process state when code is changed            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(code_change(OldVsn :: term() | {down, term()}, State :: #socket_controller{}, Extra :: term()) ->
	     {ok, NewState :: #socket_controller{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   ___      _ _   _      _ _          %%
%%  |_ _|_ _ (_) |_(_)__ _| (_)______   %%
%%   | || ' \| |  _| / _` | | |_ / -_)  %%
%%  |___|_||_|_|\__|_\__,_|_|_/__\___|  %%
%%                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


-spec(init(Args :: term()) ->
	     {ok, State :: #socket_controller{}} | {ok, State :: #socket_controller{}, timeout() | hibernate} |
	     {stop, Reason :: term()} | ignore).
init({Host, Port}) ->
    try
	DecodedJson =
	    case httpc:request("http://" ++ Host ++ ":" ++ cdtp_utils:to_str(Port) ++ "/json") of
		{ok, {{"HTTP/1.1", 200, _}, _Headers, RespBody}} ->
		    cdtp_utils:from_json(RespBody);
		{error, Reason} ->
		    ?LOG_ERROR("HTTPC ERROR: ~p~n", Reason),
		    throw({stop, {error, httpc_error}})
	    end,

	PageIdList = [proplists:get_value(<<"id">>, X) || X <- DecodedJson, binary:match(proplists:get_value(<<"url">>, X, null), ?COINBASE_URL) =/= nomatch],
	case PageIdList of
	    [PageId] ->
		{ok, Conn} = gun:open(Host, Port),
		gun:ws_upgrade(Conn, <<"/devtools/page/", PageId/binary>>),
		{ok, #socket_controller{page_id = PageId, socket_connection = Conn}};
	    _ ->
		?LOG_ERROR("Coinbase tab not found"),
		{stop, {error, coinbase_not_found}}
	end
    catch
	C:E:S ->
	    ?LOG_ERROR("ERROR IN SOCKET CONTROLLER: ~p~n", [{C, E}]),
	    {stop, {C, E, S}}
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   _  _              _ _ _              ___      _ _   __  __                               %%
%%  | || |__ _ _ _  __| | (_)_ _  __ _   / __|__ _| | | |  \/  |___ ______ __ _ __ _ ___ ___  %%
%%  | __ / _` | ' \/ _` | | | ' \/ _` | | (__/ _` | | | | |\/| / -_|_-<_-</ _` / _` / -_|_-<  %%
%%  |_||_\__,_|_||_\__,_|_|_|_||_\__, |  \___\__,_|_|_| |_|  |_\___/__/__/\__,_\__, \___/__/  %%
%%				  |___/                                         |___/         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
		  State :: #socket_controller{}) ->
	     {reply, Reply :: term(), NewState :: #socket_controller{}} |
	     {reply, Reply :: term(), NewState :: #socket_controller{}, timeout() | hibernate} |
	     {noreply, NewState :: #socket_controller{}} |
	     {noreply, NewState :: #socket_controller{}, timeout() | hibernate} |
	     {stop, Reason :: term(), Reply :: term(), NewState :: #socket_controller{}} |
	     {stop, Reason :: term(), NewState :: #socket_controller{}}).
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   _  _              _ _ _              ___         _     __  __                               %%
%%  | || |__ _ _ _  __| | (_)_ _  __ _   / __|__ _ __| |_  |  \/  |___ ______ __ _ __ _ ___ ___  %%
%%  | __ / _` | ' \/ _` | | | ' \/ _` | | (__/ _` (_-<  _| | |\/| / -_|_-<_-</ _` / _` / -_|_-<  %%
%%  |_||_\__,_|_||_\__,_|_|_|_||_\__, |  \___\__,_/__/\__| |_|  |_\___/__/__/\__,_\__, \___/__/  %%
%%                                |___/                                            |___/         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(handle_cast(Request :: term(), State :: #socket_controller{}) ->
	     {noreply, NewState :: #socket_controller{}} |
	     {noreply, NewState :: #socket_controller{}, timeout() | hibernate} |
	     {stop, Reason :: term(), NewState :: #socket_controller{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   _  _              _ _ _              ___  _   _              __  __                               %%
%%  | || |__ _ _ _  __| | (_)_ _  __ _   / _ \| |_| |_  ___ _ _  |  \/  |___ ______ __ _ __ _ ___ ___  %%
%%  | __ / _` | ' \/ _` | | | ' \/ _` | | (_) |  _| ' \/ -_) '_| | |\/| / -_|_-<_-</ _` / _` / -_|_-<  %%
%%  |_||_\__,_|_||_\__,_|_|_|_||_\__, |  \___/ \__|_||_\___|_|   |_|  |_\___/__/__/\__,_\__, \___/__/  %%
%%                                |___/                                                  |___/         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(handle_info(Info :: timeout() | term(), State :: #socket_controller{}) ->
	     {noreply, NewState :: #socket_controller{}} |
	     {noreply, NewState :: #socket_controller{}, timeout() | hibernate} |
	     {stop, Reason :: term(), NewState :: #socket_controller{}}).

handle_info(ping_ws, #socket_controller{socket_connection = C} = State) ->
    ok = gun:ws_send(C, ping),
    {noreply, State};
handle_info({gun_ws, C, _, {_, Content}}, #socket_controller{socket_connection = C} = State) ->
    DecodedFrame = cdtp_utils:from_json(Content),
    Method = proplists:get_value(<<"method">>, DecodedFrame),
    _ = handle_cdt(Method, DecodedFrame),
    {noreply, State};
handle_info({gun_upgrade, C, _, [<<"websocket">>], _}, #socket_controller{socket_connection = C} = State) ->
    ok = gun:ws_send(C, {text, <<"{\"id\":1,\"method\":\"Network.enable\",\"params\":{\"maxPostDataSize\":65536}}">>}),
    ?LOG_INFO("Connected to DevTools!~n", []),
    {ok, TRef} = timer:send_interval(30000, ping_ws),
    {noreply, State#socket_controller{timer = TRef}};
handle_info({gun_response, C, _, _, _, _}, #socket_controller{socket_connection = C}) ->
    {stop, {error, ws_upgrade_failed}};
handle_info({gun_error, C, _, _}, #socket_controller{socket_connection = C}) ->
    {stop, {error, ws_upgrade_failed}};
handle_info(_Info, State) ->
    {noreply, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This function is called by a gen_server when it is about to         %%
%% terminate. It should be the opposite of Module:init/1 and do any    %%
%% necessary cleaning up. When it returns, the gen_server terminates   %%
%% with Reason. The return value is ignored.                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
		State :: #socket_controller{}) -> term()).
terminate(_Reason, #socket_controller{timer = T}) ->
    case T of
        undefined -> noop;
        T -> timer:cancel(T)
    end,
    ok.

handle_cdt(<<"Network.webSocketFrameReceived">>, Data) ->
    Params = proplists:get_value(<<"params">>, Data),
    Response = proplists:get_value(<<"response">>, Params),
    PayloadData = proplists:get_value(<<"payloadData">>, Response),

    CoinbaseWsData = cdtp_utils:from_json(PayloadData),
    _ = handle_ws_data(proplists:get_value(<<"type">>, CoinbaseWsData), CoinbaseWsData),
    ok;

handle_cdt(_, _) -> ok.

handle_ws_data(<<"ticker">>, WsData) ->
    ProductId = proplists:get_value(<<"product_id">>, WsData),
    Price = proplists:get_value(<<"price">>, WsData),

    ok = cdtp_db:update_price(ProductId, cdtp_utils:to_bin(Price));
handle_ws_data(_, _) -> ok.
