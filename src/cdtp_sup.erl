-module(cdtp_sup).
-author("glicOne").
-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%%%%%%%%%%%%%%%%%%%%%%%
%%     _   ___ ___   %%
%%    /_\ | _ \_ _|  %%
%%   / _ \|  _/| |   %%
%%  /_/ \_\_| |___|  %%
%%                   %%
%%%%%%%%%%%%%%%%%%%%%%%

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  ___                        _                       _ _ _             _         %%
%% / __|_  _ _ __  ___ _ ___ _(_)___ ___ _ _   __ __ _| | | |__  __ _ __| |__ ___  %%
%% \__ \ || | '_ \/ -_) '_\ V / (_-</ _ \ '_| / _/ _` | | | '_ \/ _` / _| / /(_-<  %%
%% |___/\_,_| .__/\___|_|  \_/|_/__/\___/_|   \__\__,_|_|_|_.__/\__,_\__|_\_\/__/  %%
%%          |_|                                                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Child :: #{id => Id, start => {M, F, A}}                   %%
%% Optional keys are restart, shutdown, type, modules.        %%
%% Before OTP 18 tuples must be used to specify a child. e.g. %%
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

init([]) ->
    Host = application:get_env(cdtp, host, "127.0.0.1"),
    Port = application:get_env(cdtp, port, 9222),
    {ok, {
        {one_for_all, 0, 1}, [
            #{
                id => cdtp_db,
                start => {cdtp_db, start_link, []}
            },
            #{
                id => cdtp_socket_controller,
                start => {cdtp_socket_controller, start_link, [Host, Port]}
            }
        ]
    }}.

