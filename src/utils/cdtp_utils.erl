-module(cdtp_utils).
-author("glicOne").

-export([to_bin/1,
	 to_str/1,
	 from_json/1]).

-spec to_bin(binary()|list()|integer()|atom()|float()) -> binary().
to_bin(X) when is_binary(X) -> X;
to_bin(X) when is_list(X) -> list_to_binary(X);
to_bin(X) when is_integer(X) -> list_to_binary(integer_to_list(X));
to_bin(X) when is_atom(X) -> atom_to_binary(X, utf8);
to_bin(X) when is_float(X) -> list_to_binary(lists:flatten(io_lib:format("~.2f",[X]))).

-spec to_str(binary()|list()|integer()|atom()|float()) -> list().
to_str(X) when is_list(X) -> X;
to_str(X) when is_binary(X) -> binary_to_list(X);
to_str(X) when is_integer(X) -> integer_to_list(X);
to_str(X) when is_atom(X) -> atom_to_list(X);
to_str(X) when is_float(X) -> lists:flatten(io_lib:format("~.2f",[X])).

-spec from_json(list()|binary()) -> list()|{stop, {error, response_not_json}}.
from_json(Data) ->
    case {jsx:is_json(to_bin(Data)), jsx:is_json(Data)}  of
	{true, _ }->
	    jsx:decode(to_bin(Data));
	{_, true} ->
	    jsx:decode(Data);
	_ ->
	    throw({stop, {error, response_not_json}})
    end.
